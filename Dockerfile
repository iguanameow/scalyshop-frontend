FROM alpine
RUN apk add nodejs npm
WORKDIR /app
COPY package*.json ./

ARG VUE_HOST
ARG VUE_PORT
ARG VUE_PROTOCOL

ENV VUE_APP_BACKEND_HOST=$VUE_HOST
ENV VUE_APP_BACKEND_PORT=$VUE_PORT
ENV VUE_APP_BACKEND_PROTOCOL=$VUE_PROTOCOL
ENV PORT=5000

RUN echo $VUE_APP_BACKEND_HOST > .env
RUN echo $VUE_APP_BACKEND_PORT >> .env
RUN echo $VUE_APP_BACKEND_PROTOCOL >> .env

RUN npm install --force
RUN npm install -g serve
COPY . .
RUN npm run build
EXPOSE 5000
#CMD ["npm", "run", "serve"] ## Development server
CMD ["serve", "-s", "dist"]
